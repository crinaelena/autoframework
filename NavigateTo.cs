﻿using AutoFramework.UIElements;
using System.Threading;

namespace AutoFramework
{
    public static class NavigateTo
    {
        public static void LoginFormThroughTheMenu() 
        {
            var homepage = new Homepage();
            homepage.TestScenarios.Click();

            var testScenariosPage = new TestScenariosPage();
            testScenariosPage.LoginForm.Click();

        }
        public static void LoginFormThroughThePost()
        {
            var homePage = new Homepage();
            homePage.TestCases.Click();
            Thread.Sleep(500);

            var tcPage = new TestCasesPage();
            tcPage.UsernameFieldInLogin.Click();
            Thread.Sleep(500);

            UsernameFieldPost ufPost = new UsernameFieldPost();
            ufPost.UsernamePost.Click();
            Thread.Sleep(500);
        }
        public static void RegisterFormThroughTheMenu() 
        {
            var homepage = new Homepage();
            homepage.TestScenarios.Click();

            var testScenarios = new TestScenariosPage();
            testScenarios.RegisterForm.Click();
        }

    }
}
