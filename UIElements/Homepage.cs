﻿using OpenQA.Selenium;

namespace AutoFramework.UIElements
{
    public class Homepage
    {
        public IWebElement Introduction = Driver.driver.FindElement(By.Id("menu-item-25"));

        public IWebElement Selectors = Driver.driver.FindElement(By.Id("menu-item-106"));

        public IWebElement SpecialElements = Driver.driver.FindElement(By.Id("menu-item-35"));

        public IWebElement TestCases = Driver.driver.FindElement(By.Id("menu-item-57"));

        public IWebElement TestScenarios = Driver.driver.FindElement(By.Id("menu-item-58"));

        public IWebElement About = Driver.driver.FindElement(By.Id("menu-item-26"));

        public IWebElement LogIn = Driver.driver.FindElement(By.LinkText("Log in"));
    }
}
