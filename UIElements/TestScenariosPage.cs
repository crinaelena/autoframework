using OpenQA.Selenium;

namespace AutoFramework
{
    public class TestScenariosPage
    {
        public IWebElement LoginForm = Driver.driver.FindElement(By.CssSelector("#main-content > article.mh-loop-item.mh-clearfix.post-72.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-test-scenarios > figure > a > img"));
        
        public IWebElement RegisterForm = Driver.driver.FindElement(By.CssSelector("#main-content > article.mh-loop-item.mh-clearfix.post-70.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-test-scenarios > div > header > h3 > a"));
    }
}
