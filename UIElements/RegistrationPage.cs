﻿using OpenQA.Selenium;

namespace AutoFramework.UIElements
{
    public class RegistrationPage
    { 

        public IWebElement UserId = Driver.driver.FindElement(By.Name("userid"));
        public IWebElement UserPassword = Driver.driver.FindElement(By.Name("passid"));
        public IWebElement Name = Driver.driver.FindElement(By.Name("username"));
        public IWebElement Adress = Driver.driver.FindElement(By.Name("address"));
        public IWebElement CountrySelector = Driver.driver.FindElement(By.CssSelector("#post-70 > div > form > ul > li:nth-child(10) > select"));
        public IWebElement Country = Driver.driver.FindElement(By.CssSelector("#post-70 > div > form > ul > li:nth-child(10) > select > option:nth-child(2)"));
        public IWebElement ZipCode = Driver.driver.FindElement(By.Name("zip"));
        public IWebElement Email = Driver.driver.FindElement(By.Name("email"));
        public IWebElement Genre = Driver.driver.FindElement(By.CssSelector("#post-70 > div > form > ul > li:nth-child(17) > input[type=radio]"));
        public IWebElement SpeaksEnglish = Driver.driver.FindElement(By.CssSelector("#post-70 > div > form > ul > li:nth-child(19) > input[type=checkbox]"));
        public IWebElement Description = Driver.driver.FindElement(By.Id("desc"));
        public IWebElement RegisterButton = Driver.driver.FindElement(By.Name("submit"));

        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed;
        }
    }
}
