﻿using OpenQA.Selenium;

namespace AutoFramework.UIElements
{
    public class LoginPage
    {
        public IWebElement UsernameField = Driver.driver.FindElement(By.Name("userid"));

        public IWebElement PasswordField = Driver.driver.FindElement(By.Name("passid"));

        public IWebElement RepeatPasswordField = Driver.driver.FindElement(By.Name("repeatpassid"));

        public IWebElement SubmitBtn = Driver.driver.FindElement(By.Name("submit"));
    }
}
