using OpenQA.Selenium;

namespace AutoFramework.UIElements
{
    public class TestCasesPage
    {
        public IWebElement UsernameFieldInLogin = Driver.driver.FindElement(By.CssSelector("#main-content > article.mh-loop-item.mh-clearfix.post-76.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-test-cases > div > header > h3 > a"));
        public IWebElement CategoryDropDownMenu = Driver.driver.FindElement(By.CssSelector("#main-content > article.mh-loop-item.mh-clearfix.post-78.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-test-cases > div > header > h3 > a"));
    }
}
