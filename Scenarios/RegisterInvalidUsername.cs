﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AutoFramework.Scenarios
{
    public class RegisterInvalidUsername
    {
        IAlert alert;
        [OneTimeSetUp]
        public void Initialize() 
        {
            UserActions.InitializeDriver();
            NavigateTo.RegisterFormThroughTheMenu();
        }
        [Test]
        public void LessThanFourCharacters() 
        {
            UserActions.FillRegistrationForm(Config.Credentials.Invalid.Username.FourCharacters, Config.Credentials.Valid.Password,
                Config.Credentials.Valid.Name,Config.Address,Config.ZipCode,Config.Credentials.Valid.Email,Config.Description);
            Thread.Sleep(1000);
            alert = Driver.driver.SwitchTo().Alert();

            Assert.AreEqual(Config.AlertMessages.UserErrorMessage, alert.Text);
            alert.Accept();
        }
        [Test]
        public void MoreThanThirteenCharacters()
        {
            UserActions.FillRegistrationForm(Config.Credentials.Invalid.Username.ThirteenCharacters, Config.Credentials.Valid.Password,
                Config.Credentials.Valid.Name, Config.Address, Config.ZipCode, Config.Credentials.Valid.Email, Config.Description);
            Thread.Sleep(1000);
            alert = Driver.driver.SwitchTo().Alert();

            Assert.AreEqual(Config.AlertMessages.UserErrorMessage, alert.Text);
            alert.Accept();
        }

        [OneTimeTearDown]
        public void CleanUp() 
        {
            Driver.driver.Quit();
        }
    }
}
