﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFramework.Scenarios
{
    public class LoginInvalidUsername
    {
        IAlert alert;
        [OneTimeSetUp]
        public void Initialize()
        {
            UserActions.InitializeDriver();
            NavigateTo.LoginFormThroughTheMenu();
        }

        [Test]
        public void LessThan5Chars()
        {
            UserActions.FillLoginForm(Config.Credentials.Invalid.Username.FourCharacters,
                Config.Credentials.Valid.Password, Config.Credentials.Valid.RepeatPassword);

            alert = Driver.driver.SwitchTo().Alert();

            Assert.AreEqual(Config.AlertMessages.UserErrorMessage, alert.Text);
            alert.Accept();
        }
        [Test]
        public void MoreThan12Chars()
        {
            UserActions.FillLoginForm(Config.Credentials.Invalid.Username.ThirteenCharacters,
                Config.Credentials.Valid.Password, Config.Credentials.Valid.RepeatPassword);

            alert = Driver.driver.SwitchTo().Alert();

            Assert.AreEqual(Config.AlertMessages.UserErrorMessage, alert.Text);
            alert.Accept();
        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            Driver.driver.Quit();
        }
    }
}
