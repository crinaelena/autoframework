﻿using AutoFramework.UIElements;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AutoFramework.Scenarios
{
   public class RegisterValidCredentials
    {
        
        [SetUp]
        public void Initialize() 
        {
            UserActions.InitializeDriver();
            NavigateTo.RegisterFormThroughTheMenu();
        }
        [Test]
        public void ValidCredentials() 
        {
            UserActions.FillRegistrationForm(Config.Credentials.Valid.Username, Config.Credentials.Valid.Password,
                Config.Credentials.Valid.Name, Config.Address, Config.ZipCode, Config.Credentials.Valid.Email, Config.Description);


            IWebElement header = Driver.driver.FindElement(By.CssSelector("#post-70 > header > h1"));
            var element = new RegistrationPage();
           
            Assert.True(element.IsElementVisible(header));
            Thread.Sleep(1000);
            
        }
        [TearDown]
        public void CleanUp() 
        {
            Driver.driver.Quit();
        }
    }
}
