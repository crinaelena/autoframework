﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace AutoFramework.Scenarios
{
    public class LoginInvalidPassword
    {
        IAlert alert;
        [OneTimeSetUp]
        public void Initialize() 
        {
            UserActions.InitializeDriver();
            NavigateTo.LoginFormThroughTheMenu();
        }
        [Test]
        public void LessThanFourCharacters()
        {
            UserActions.FillLoginForm(Config.Credentials.Valid.Username, Config.Credentials.Invalid.Password.FourCharacters, Config.Credentials.Invalid.Password.FourCharacters);

            alert = Driver.driver.SwitchTo().Alert();
            Assert.AreEqual(Config.AlertMessages.PassErrorMessage,alert.Text);

            alert.Accept();
        }
        [Test]
        public void MoreThan12Chars()
        {
            UserActions.FillLoginForm(Config.Credentials.Valid.Username, Config.Credentials.Invalid.Password.ThirteenCharacters,
                Config.Credentials.Invalid.Password.ThirteenCharacters);

            alert = Driver.driver.SwitchTo().Alert();

            Assert.AreEqual(Config.AlertMessages.PassErrorMessage, alert.Text);
            alert.Accept();
        }
        [OneTimeTearDown]
        public void CleanUp() 
        {
            Driver.driver.Quit();
        }
    }
}
