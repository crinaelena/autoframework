﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFramework
{
    public static class Config
    {
        public static string BaseURL = "http://testing.todorvachev.com";

        public static string Address = "Theaddress";
        public static string ZipCode = "123456";
        public static string Description = "This is the description.";

        public static class Credentials
        {
            public static class Valid
            {
                public static string Name = "TheNameofTheUser";
                public static string Username = "ValidUser";
                public static string Password = "asdf1234!";
                public static string RepeatPassword = "asdf1234!";
                public static string Email = "example@example.com";
            }
            public static class Invalid
            {
                public static class Username
                {
                    public static string FourCharacters = "Asdf";
                    public static string ThirteenCharacters = "AsdfAsdfAsdfAsdfg";
                }
                public static class Password
                {
                    public static string FourCharacters = "asdf";
                    public static string ThirteenCharacters = "asdfasdfasdfasdf1";
                }
                public static class Email
                {
                    public static string FormatInvalid = "example";
                }
            }
        }
        public static class AlertMessages
        {
            public static string SuccessfulLogin = "Succesful login!";
            public static string UserErrorMessage = "User Id should not be empty / length be between 5 to 12";
            public static string PassErrorMessage = "Password should not be empty / length be between 5 to 12";
        }
    }
}
