﻿using AutoFramework.UIElements;
using OpenQA.Selenium.Support.UI;

namespace AutoFramework
{
   public static class UserActions
    {
        public static void InitializeDriver() 
        {
            Driver.driver.Navigate().GoToUrl(Config.BaseURL);
        }
        public static void FillLoginForm(string username, string password, string repeatPassword) 
        {
            LoginPage lsPost = new LoginPage();

            lsPost.UsernameField.Clear();
            lsPost.UsernameField.SendKeys(username);

            lsPost.PasswordField.Clear();
            lsPost.PasswordField.SendKeys(password);

            lsPost.RepeatPasswordField.Clear();
            lsPost.RepeatPasswordField.SendKeys(repeatPassword);
            lsPost.SubmitBtn.Click();
        }
        public static void FillRegistrationForm(string user, string password, string name, string adress, string zip, string email, string description) 
        {
            var register = new RegistrationPage();
            
            register.UserId.SendKeys(user);
            register.UserPassword.SendKeys(password);
            register.Name.SendKeys(name);
            register.Adress.SendKeys(adress);
            register.CountrySelector.Click();
            
            var selectElement = new SelectElement(register.CountrySelector);
           
            selectElement.SelectByValue("AF");
            register.ZipCode.SendKeys(zip);
            register.Email.SendKeys(email);
            register.Genre.Click();
            register.SpeaksEnglish.Click();
            register.Description.SendKeys(description);
            register.RegisterButton.Click();

        }
    }
}
